package com.paywithbytes.jfec.file.wrapper.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.Test;

import com.paywithbytes.jfec.exceptions.DecodingException;
import com.paywithbytes.jfec.exceptions.EncodingException;
import com.paywithbytes.jfec.file.wrapper.FileDecodeWrapper;
import com.paywithbytes.jfec.file.wrapper.FileEncodeWrapper;
import com.paywithbytes.jfec.wrapper.Chunk;

/**
 * 
 * In this class we test the FileEncodeWrapper and the FileDecodeWrapper class.
 * 
 * You should have a file named "testFile.txt" in a folder named "TestFiles".
 * 
 * In that same folder, a new file is going to be created from the chunks
 * obtained from the "testFile.txt". This new file will be named
 * testFileRecovered.txt.
 * 
 */
public class FileEncodeDecodeWrapperTest {

	private static final String originalFilePath = "TestFiles/testFile.txt";
	private static final String recoveredFilePath = "TestFiles/testFileRecovered.txt";

	@Test
	public void testEncodingOfFiles() throws EncodingException, DecodingException, NoSuchAlgorithmException,
			IOException {

		FileEncodeWrapper few = new FileEncodeWrapper(originalFilePath, 10, 4);
		Chunk[] fullChunks = few.encodeFromFile();

		Chunk[] reducedSetOfChunks = new Chunk[4];
		reducedSetOfChunks[0] = fullChunks[1];
		reducedSetOfChunks[1] = fullChunks[4];
		reducedSetOfChunks[2] = fullChunks[7];
		reducedSetOfChunks[3] = fullChunks[9];

		FileDecodeWrapper fdw = new FileDecodeWrapper(reducedSetOfChunks, recoveredFilePath, few.getMetadata());

		fdw.decodeToFile();

		MessageDigest md5 = MessageDigest.getInstance("MD5");

		byte[] originalFileMD5 = md5.digest(Files.readAllBytes(Paths.get(originalFilePath)));
		byte[] recoveredFileMD5 = md5.digest(Files.readAllBytes(Paths.get(recoveredFilePath)));

		Assert.assertArrayEquals("The files should be equals! If you got an error at this point, "
				+ "check whether the paths of the file are correct and that you have permission"
				+ " to write in that directory", originalFileMD5, recoveredFileMD5);

	}
}
