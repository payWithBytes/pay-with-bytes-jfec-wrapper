package com.paywithbytes.jfec.wrapper.test;

import org.junit.Assert;
import org.junit.Test;

import com.paywithbytes.jfec.exceptions.DecodingException;
import com.paywithbytes.jfec.exceptions.EncodingException;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.jfec.wrapper.FecEncodeWrapper;

/**
 * Run this tests, if all of them work properly then you can "safely" use the
 * library.
 * 
 */
public class FecEncodeWrapperTest {

	@Test
	public void test2XRedundancy() throws DecodingException, EncodingException {
		// The data to encode (u2 lyrics)
		String strVal = "You got to leave it behind, All that you fashion, All that you make, "
				+ "All that you build, All that you break, All this you can leave behind..";

		EncodingMetadata metadata = new EncodingMetadata(6, 3, strVal.length());
		FecEncodeWrapper encode = new FecEncodeWrapper(metadata);

		// Convert to bytes.
		byte[] source = strVal.getBytes();

		// The N chunks encoded
		Chunk[] fullChunkArray = encode.encode(source);

		// This will hold the K chunks that we are going to use for decoding.
		Chunk[] reducedChunkArray = new Chunk[metadata.getK()];

		// Simulate dropping half of the packets
		int j = 0;
		for (int i = 0; i < fullChunkArray.length; i++) {
			if (i % 2 == 1)
				continue;

			reducedChunkArray[j] = fullChunkArray[i];
			j++;
		}

		// Decode
		byte[] received = encode.decode(reducedChunkArray);

		Assert.assertArrayEquals("Source and Received Files are NOT equal!", source, received);

	}

	@Test
	public void testLessThan2XRedundancy() throws EncodingException, DecodingException {
		// The data to encode (u2 lyrics)
		String strVal = "And love is not the easy thing, The only baggage that you can bring... "
				+ "And love is not the easy thing... The only baggage you can bring  Is all that you can't leave behind";

		EncodingMetadata metadata = new EncodingMetadata(7, 4, strVal.length());
		FecEncodeWrapper encode = new FecEncodeWrapper(metadata);

		// Convert to bytes.
		byte[] source = strVal.getBytes();

		// The N chunks encoded
		Chunk[] fullChunkArray = encode.encode(source);

		// This will hold the K chunks that we are going to use for decoding.
		Chunk[] reducedChunkArray = new Chunk[metadata.getK()];

		// Simulate dropping 3 of the 7 packets
		int j = 0;
		for (int i = 0; i < fullChunkArray.length; i++) {
			if (i % 2 == 1)
				continue;

			reducedChunkArray[j] = fullChunkArray[i];
			j++;
		}

		// Decode
		byte[] received = encode.decode(reducedChunkArray);

		Assert.assertArrayEquals("Source and Received Files are NOT equal!", source, received);
	}

	@Test
	public void testMoreThan2XRedundancy() throws EncodingException, DecodingException {
		// The data to encode (u2 lyrics)
		String strVal = "And love is not the easy thing, The only baggage that you can bring... "
				+ "And love is not the easy thing... The only baggage you can bring  "
				+ "Is all that you can't leave behind";

		EncodingMetadata metadata = new EncodingMetadata(9, 4, strVal.length());
		FecEncodeWrapper encode = new FecEncodeWrapper(metadata);

		// Convert to bytes.
		byte[] source = strVal.getBytes();

		// The N chunks encoded
		Chunk[] fullChunkArray = encode.encode(source);

		// This will hold the K chunks that we are going to use for decoding.
		Chunk[] reducedChunkArray = new Chunk[metadata.getK()];

		// Simulate dropping 4 of the 9 packets
		int j = 0;
		for (int i = 0; i < fullChunkArray.length; i++) {
			if (i % 2 == 1 || j >= metadata.getK())
				continue;

			reducedChunkArray[j] = fullChunkArray[i];
			j++;
		}

		// Decode
		byte[] received = encode.decode(reducedChunkArray);

		Assert.assertArrayEquals("Source and Received Files are NOT equal!", source, received);
	}

}
