package com.paywithbytes.jfec.wrapper;

import com.paywithbytes.jfec.exceptions.MetadataException;

/**
 * This class is need it for encoding and decoding a series of Chunks.
 * 
 * After sending the Chunks to different devices/computers we will also need to
 * store this information in somewhere safe. Without this metadata information
 * the decoding will not be possible.
 * 
 */
public class EncodingMetadata {

	/**
	 * 
	 * This class contains all the necessary metadata to encode and decode a
	 * given file.
	 * 
	 * @param n
	 *            the total amount of chunks by which the file is going to be
	 *            split it.
	 * @param k
	 *            the total amount of chunks need it to reconstruct the original
	 *            file.
	 * @param fileSize
	 *            the size of the file in bytes.
	 */
	public EncodingMetadata(int n, int k, long fileSize) {

		this.n = n;
		this.k = k;

		if (n <= k) {
			throw new MetadataException("N should be greater than K in order to encode properly.");
		}

		// In order to recover a file properly, this should be true (k *
		// chunkSize > fileSize)
		this.chunkSize = (int) ((fileSize / this.getK()) + 1);

		if (this.getChunkSize() * this.getK() <= fileSize) {
			throw new MetadataException("The chunk size was not calculated properly (k * chunkSize > fileSize).");
		}

		int exactAmountOfChunks = (int) (fileSize / chunkSize + 1);
		this.paddingBytesCount = (int) (exactAmountOfChunks * chunkSize - fileSize);
	}

	private final int n;
	private final int k;
	private final int chunkSize;
	private final int paddingBytesCount;

	public int getN() {
		return n;
	}

	public int getK() {
		return k;
	}

	public int getChunkSize() {
		return chunkSize;
	}

	public int getPaddingBytesCount() {
		return paddingBytesCount;
	}

	@Override
	public boolean equals(Object o) {

		// TODO: check for null values, this could become a bug! Fix this.
		EncodingMetadata other = (EncodingMetadata) o;

		if (this.getChunkSize() == other.getChunkSize() && this.getK() == other.getK() && this.getN() == other.getN()
				&& this.getPaddingBytesCount() == other.getPaddingBytesCount()) {
			return true;
		}

		return false;
	}

}
