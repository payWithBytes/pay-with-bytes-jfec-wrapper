package com.paywithbytes.jfec.wrapper;

import com.onionnetworks.fec.FECCode;
import com.onionnetworks.fec.FECCodeFactory;
import com.onionnetworks.util.Buffer;
import com.paywithbytes.jfec.exceptions.DecodingException;
import com.paywithbytes.jfec.exceptions.EncodingException;

/**
 * This class is a Wrapper of the FEC encode functionalities supported by the
 * onionnetworks code. I have adapted this to my needs from this project:
 * 
 * https://github.com/jc/javafec-example
 * 
 */
public class FecEncodeWrapper {

	public FecEncodeWrapper(EncodingMetadata metadata) {
		this.metadata = metadata;
	}

	private final EncodingMetadata metadata;

	private EncodingMetadata getMetadata() {
		return metadata;
	}

	/**
	 * Encode the source received as a parameter into an array of N Chunks. This
	 * Chunk objects can be sent to another devices/computers to add redundancy.
	 * 
	 * Only K of this Chunks (where K < N) are necessary to restore the source.
	 * 
	 * @param source
	 *            the bytes of data you want to encode
	 * @return
	 * @throws EncodingException
	 */
	public Chunk[] encode(byte[] source) throws EncodingException {

		// Add padding at the end of the source.
		/*
		 * NOTE:This is because the source needs to split into k*chunkSize
		 * sections. If the file is not of the right size, then we add padding
		 * at the end of the source. We also remove the padding after decoding.
		 * All of this metadata information is stored in the EncodingMetadata
		 * class, so you should store that info to be able to decode the file
		 * properly afterwards.
		 */
		if (source.length % this.getMetadata().getChunkSize() != 0) {
			source = addPadding(source, this.getMetadata().getChunkSize());
		}

		// This will hold the encoded file, (n * chunkSize) bytes
		byte[] repair = new byte[this.getMetadata().getN() * this.getMetadata().getChunkSize()];

		// These buffers allow us to put our data in them they
		// reference a packet length of the file (or at least will once we fill
		// them)
		Buffer[] sourceBuffer = new Buffer[this.getMetadata().getK()];
		Buffer[] repairBuffer = new Buffer[this.getMetadata().getN()];

		for (int i = 0; i < sourceBuffer.length; i++)
			sourceBuffer[i] = new Buffer(source, i * this.getMetadata().getChunkSize(), this.getMetadata()
					.getChunkSize());

		for (int i = 0; i < repairBuffer.length; i++)
			repairBuffer[i] = new Buffer(repair, i * this.getMetadata().getChunkSize(), this.getMetadata()
					.getChunkSize());

		// When sending the data you must identify what it's index was.
		// Will be shown and explained later
		int[] repairIndex = new int[this.getMetadata().getN()];

		for (int i = 0; i < repairIndex.length; i++)
			repairIndex[i] = i;

		try {
			// Create our fec code
			FECCode fec = FECCodeFactory.getDefault().createFECCode(this.getMetadata().getK(),
					this.getMetadata().getN());

			// Encode the data
			fec.encode(sourceBuffer, repairBuffer, repairIndex);
			// encoded data is now contained in the repairBuffer/repair byte
			// array
		} catch (Exception e) {
			throw new EncodingException("An error has ocurred while calling the onionnetworks fec encoding function", e);
		}
		// From here you can send each 'Chunk' of the encoded data, along with
		// what repairIndex it has (contained in the Chunk class). Also include
		// the group number if you had to
		// split the file
		Chunk[] chunkArray = new Chunk[this.getMetadata().getN()];
		for (int i = 0; i < this.getMetadata().getN(); i++) {
			Buffer item = repairBuffer[i];
			chunkArray[i] = new Chunk(item.getBytes(), item.off / this.getMetadata().getChunkSize());
		}

		return chunkArray;
	}

	/**
	 * Receives an array of exactly K chunks, rebuilds the source and returns
	 * that in an array of bytes.
	 * 
	 * @param chunkArray
	 * @return
	 * @throws DecodingException
	 */
	public byte[] decode(Chunk[] chunkArray) throws DecodingException {

		if (chunkArray.length != this.getMetadata().getK()) {
			String exceptionDescription = String.format("The chunkArray provided has [%d] elements, and K = [%d]."
					+ "You need exactly K chunks to decode a given file, no more, no less", chunkArray.length, this
					.getMetadata().getK());

			throw new DecodingException(exceptionDescription);
		}

		Buffer[] repairBuffer = buildRepairBuffer(chunkArray);

		// We only need to store k, packets received
		// Don't forget we need the index value for each packet too (this
		// information is in the Chunk objects)
		Buffer[] receiverBuffer = new Buffer[this.getMetadata().getK()];
		int[] receiverIndex = new int[this.getMetadata().getK()];

		// This will store the received packets to be decoded
		byte[] received = new byte[this.getMetadata().getK() * this.getMetadata().getChunkSize()];

		// Copy into the received buffer
		int j = 0;
		for (int i = 0; i < this.getMetadata().getN(); i++) {
			// if the repair buffer[i] is null, we just continue. This should be
			// the case some times
			// because we only need K chunks, not N chunks.
			if (repairBuffer[i] == null)
				continue;

			byte[] packet = repairBuffer[i].getBytes();
			System.arraycopy(packet, 0, received, j * this.getMetadata().getChunkSize(), packet.length);
			receiverIndex[j] = i;
			j++;
		}

		// Create our Buffers for the encoded data
		for (int i = 0; i < this.getMetadata().getK(); i++)
			receiverBuffer[i] = new Buffer(received, i * this.getMetadata().getChunkSize(), this.getMetadata()
					.getChunkSize());

		try {
			// Finally we can decode
			FECCode fec = FECCodeFactory.getDefault().createFECCode(this.getMetadata().getK(),
					this.getMetadata().getN());
			fec.decode(receiverBuffer, receiverIndex);
		} catch (Exception e) {
			throw new DecodingException("An error has occurred while trying to decode (from the onionnetworks API)", e);
		}

		return this.removePadding(received);
	}

	/**
	 * 
	 * We need to add some padding bytes when the content of the source is not
	 * exactly divisible by the size of the chunk.
	 * 
	 * We fill with 0's all of the padding bytes, and there is another function
	 * to delete this padding bytes in order to obtain the exact same data we
	 * encoded before.
	 * 
	 * @param source
	 * @param chunkSize
	 * @return
	 */
	private byte[] addPadding(byte[] bytesToSend, int chunkSize) {

		int exactAmountOfChunks = bytesToSend.length / chunkSize + 1;

		byte[] sourceWithPadding = new byte[exactAmountOfChunks * chunkSize];

		/*
		 * No need to fill in with zeroes. Thanks Java:
		 * http://docs.oracle.com/javase
		 * /specs/jls/se7/html/jls-4.html#jls-4.12.5 "Each class variable,
		 * instance variable, or array component is initialized with a default
		 * value when it is created (15.9, 15.10): For type byte, the default
		 * value is zero, that is, the value of (byte)0."
		 */

		System.arraycopy(bytesToSend, 0, sourceWithPadding, 0, bytesToSend.length);

		return sourceWithPadding;
	}

	/**
	 * 
	 * @param bytesReceived
	 * @param metadata
	 * @return
	 */
	private byte[] removePadding(byte[] bytesReceived) {

		byte[] retVal = new byte[this.getMetadata().getChunkSize() * this.getMetadata().getK()
				- this.getMetadata().getPaddingBytesCount()];

		if (bytesReceived.length != retVal.length) {
			System.arraycopy(bytesReceived, 0, retVal, 0, retVal.length);
			return retVal;
		}

		return bytesReceived;
	}

	/**
	 * Convert the Chunk array to the proper datatype in order to use the
	 * onionnetwork API.
	 * 
	 * @param chunkArray
	 * @return
	 */
	private Buffer[] buildRepairBuffer(Chunk[] chunkArray) {

		Buffer[] repairBuffer = new Buffer[this.getMetadata().getN()];

		for (Chunk chunkItem : chunkArray) {
			int i = chunkItem.getRepairIndex();
			repairBuffer[i] = new Buffer(chunkItem.getData(), 0, this.getMetadata().getChunkSize());
		}
		return repairBuffer;
	}

}
