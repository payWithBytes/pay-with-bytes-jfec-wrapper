package com.paywithbytes.jfec.wrapper;

/**
 * This class contains the information we need to pass to the different
 * devices/computers in order to retrieve them and restored the complete file
 * afterwards.
 * 
 * Besides the data of every chunk, we will also need the corresponding Metadata
 * that was used for the encoding.
 * 
 */
public class Chunk {

	public Chunk(byte[] data, int repairIndex) {
		this.data = data;
		this.repairIndex = repairIndex;
	}

	private byte[] data;
	private final int repairIndex;

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public int getRepairIndex() {
		return repairIndex;
	}

}
