package com.paywithbytes.jfec.file.wrapper;

import java.io.FileOutputStream;
import java.io.IOException;

import com.paywithbytes.jfec.exceptions.DecodingException;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.jfec.wrapper.FecEncodeWrapper;

/**
 * This class is a wrapper to decode to a file from a Chunk array.
 * 
 */
public class FileDecodeWrapper {

	public FileDecodeWrapper(Chunk[] chunkArray, String filePath, EncodingMetadata metadata) {
		this.filePath = filePath;
		this.metadata = metadata;
		this.fecEncodeWrapper = new FecEncodeWrapper(this.getMetadata());
		this.chunkArray = chunkArray;
	}

	private final FecEncodeWrapper fecEncodeWrapper;
	private final EncodingMetadata metadata;
	private final String filePath;
	private final Chunk[] chunkArray;

	private Chunk[] getChunkArray() {
		return chunkArray;
	}

	public FecEncodeWrapper getFecEncodeWrapper() {
		return fecEncodeWrapper;
	}

	public EncodingMetadata getMetadata() {
		return metadata;
	}

	private String getFilePath() {
		return filePath;
	}

	public void decodeToFile() throws DecodingException {

		try {
			byte[] source = this.getFecEncodeWrapper().decode(this.getChunkArray());

			FileOutputStream fos = new FileOutputStream(this.getFilePath());
			fos.write(source);
			fos.close();

		} catch (IOException e) {
			throw new DecodingException("IOException when trying to write bytes to the file, is the filePath correct?",
					e);
		}

	}

	public static void main(String args[]) {

	}

}
