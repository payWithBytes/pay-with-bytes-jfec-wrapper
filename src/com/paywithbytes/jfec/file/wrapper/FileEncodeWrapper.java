package com.paywithbytes.jfec.file.wrapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.paywithbytes.jfec.exceptions.EncodingException;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.jfec.wrapper.FecEncodeWrapper;

/**
 * This class is a wrapper that receives a filePath with some information (N and
 * K) and return a Chunk array.
 */
public class FileEncodeWrapper {

	public FileEncodeWrapper(String filePath, int n, int k) {
		this.filePath = filePath;
		File fp = new File(this.getFilePath());
		this.metadata = new EncodingMetadata(n, k, fp.length());
		this.fecEncodeWrapper = new FecEncodeWrapper(this.getMetadata());
	}

	private final FecEncodeWrapper fecEncodeWrapper;
	private final EncodingMetadata metadata;
	private final String filePath;

	public FecEncodeWrapper getFecEncodeWrapper() {
		return fecEncodeWrapper;
	}

	public EncodingMetadata getMetadata() {
		return metadata;
	}

	private String getFilePath() {
		return filePath;
	}

	/**
	 * When using this method, there is an implicit assumption that the machine
	 * can hold the entire file in RAM.
	 * 
	 * @return an array of the partitioned chunks.
	 * @throws EncodingException
	 */
	public Chunk[] encodeFromFile() throws EncodingException {

		Path path = Paths.get(filePath);

		try {
			// TODO: We are assuming that the computer has enough memory to hold
			// the entire file in the RAM.
			byte[] source = Files.readAllBytes(path);

			return this.getFecEncodeWrapper().encode(source);
		} catch (IOException e) {
			throw new EncodingException(
					"IOException when trying to read bytes from the file, is the filePath correct?", e);
		}

	}

}
