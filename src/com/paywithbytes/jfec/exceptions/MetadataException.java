package com.paywithbytes.jfec.exceptions;

public class MetadataException extends RuntimeException {

	/**
	 * Exceptions when setting up the metadata for the encoding/decoding.
	 */
	private static final long serialVersionUID = 1L;

	public MetadataException(){ 
		super();
	}
	
	public MetadataException(String message){
		super(message);
	}
	
	public MetadataException(String message, Throwable cause){
		super(message, cause);
	}
	
	public MetadataException(Throwable cause){
		super(cause); 
	}


}
