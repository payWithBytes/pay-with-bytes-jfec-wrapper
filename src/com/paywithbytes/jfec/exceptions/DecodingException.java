package com.paywithbytes.jfec.exceptions;

public class DecodingException extends Exception {

	/**
	 * Exceptions when decoding
	 */
	private static final long serialVersionUID = 1L;

	public DecodingException(){ 
		super();
	}
	
	public DecodingException(String message){
		super(message);
	}
	
	public DecodingException(String message, Throwable cause){
		super(message, cause);
	}
	
	public DecodingException(Throwable cause){
		super(cause); 
	}
	
}
