package com.paywithbytes.jfec.exceptions;

public class EncodingException extends Exception {

	/**
	 * Exceptions when encoding
	 */
	private static final long serialVersionUID = 1L;

	public EncodingException(){ 
		super();
	}
	
	public EncodingException(String message){
		super(message);
	}
	
	public EncodingException(String message, Throwable cause){
		super(message, cause);
	}
	
	public EncodingException(Throwable cause){
		super(cause); 
	}
	
}
