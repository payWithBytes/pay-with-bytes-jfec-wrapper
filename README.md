This project is a Java Wrapper to the Java FEC (Forward Error Correction) algorithm implemented by the Onion Networks development team.

Bitbucket link of their source code: https://bitbucket.org/onionnetworks/fec 

This is what you are going to be able to solve from this project.

- From a File or a byte array, you can create a series of N chunks (partitions of the file or byte array). This chunks have redundancy, and you only need K of them (where K < N ) to reconstruct the original file (or byte array).


I have also found another wrapper of that Java Library in this GitHub repository https://github.com/jc/javafec-example 

Some of my code is based on that Java project, but since it didn't adjust properly to my needs I have to re-writed almost entirely.

Why do we need a wrapper from this project?

- A simpler API for creating N redundant chunks from a file or from a byte array.

- From this N chunks, we have a simple API that receives K chunk (K < N) and rebuilds the entire file (or byte array).

- Encoding and decoding is simpler, and all the logic is abstract from the developer.

- Developed an organised, easy to test API. We have written a couple of JUNIT test classes in order to make it easy to run it.


In order to understand a little more what is FEC, I copied this from the FEC onion networks site:

"Forward Error Correction (FEC) is an essential building block of any satellite or IP multicast based content distribution system. Our library is the fastest and most mature Java FEC library available. It features: Fast multi-threaded I/O routines for encoding and decoding files Native linux, solaris, and win32 accellerators with pure Java fallback FEC codec plugin interface Cryptograhic hashes can be used for checking file integrity"

This project was developed in Eclipse-Kepler and Java 7.

If you just want to use this library, you can go to the download section and download these 2 files ( check for the latest version):

1. The library jar: payWithBytes-jfecWrapper.jar
2. The documentation jar: payWithBytes-jfecWrapper-javadoc.jar

Furthermore, if you want a simple example you can download a very simple sample project:

1. payWithBytes-jfecWrapper-0.3-testJars.zip


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.






